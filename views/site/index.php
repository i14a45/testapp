<?php

use app\models\UploadFileForm;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\View;


/* @var $this View */
/* @var $model UploadFileForm */
/* @var $data array */


$this->title = 'Test app';
?>
<div class="site-index">
    <div class="row">
        <div class="col-md-12">
            <?php $form = ActiveForm::begin(['id' => 'file-form']) ?>

            <?= $form->field($model, 'file', [
                'options' => [
                    'class' => 'input-group',
                ],
                'labelOptions' => [
                    'class' => 'custom-file-label',
                ],
                'template' => "<div class=\"custom-file\">{input}\n{label}</div><div class=\"input-group-append\">" .
                        Html::submitButton('Загрузить', ['class' => 'btn btn-primary']) .
                    "</div>\n{error}",
            ])->fileInput([
                'class' => 'custom-file-input',
            ]) ?>

            <?php ActiveForm::end() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 mt-5">
            <div id="chart" style="width:100%;height:300px"></div>
        </div>
    </div>
</div>

<?php
if (!empty($data)) {
    $dataJson = Json::encode($data);

    $this->registerJsFile('https://www.gstatic.com/charts/loader.js', [
        'position' => View::POS_HEAD,
    ]);
    $this->registerJs(
<<<JS
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable($dataJson)

        var options = {
          title: 'Изменение баланса',
          curveType: 'function',
          legend: { position: 'bottom' }
        };

        var chart = new google.visualization.LineChart(document.getElementById('chart'));

        chart.draw(data, options);
      }

JS
,View::POS_HEAD);
}

$this->registerJs(
<<<JS
    $('#file-form').on('afterValidate', function(event, messages, errorAttributes){
        if (errorAttributes.length > 0) {
            $('.invalid-feedback').show();
        } else {
            $('.invalid-feedback').hide();
        }        
    });    

    bsCustomFileInput.init();
JS
);
<?php


namespace app\models;


use yii\base\Model;
use yii\web\UploadedFile;

/**
 * Class UploadFileForm
 * @package app\models
 */
class UploadFileForm extends Model
{
    /** @var UploadedFile */
    public $file;

    /** @var string */
    private $_filePath;

    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        return [
            [['file'], 'required'],
            [['file'], 'file', 'extensions' => ['html']],
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function attributeLabels()
    {
        return [
            'file' => 'Файл',
        ];
    }

    /**
     * Загрузка файла
     *
     * @return bool
     */
    public function upload()
    {
        if ($this->validate()) {
            $this->_filePath = 'uploads/' . $this->file->baseName . '.' . $this->file->extension;
            return $this->file->saveAs($this->_filePath);
        }

        return false;
    }

    /**
     * @return string
     */
    public function getFilePath()
    {
        return $this->_filePath;
    }
}
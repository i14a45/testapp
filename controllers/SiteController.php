<?php

namespace app\controllers;

use app\components\Parser;
use app\models\UploadFileForm;
use Yii;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\ErrorAction;
use yii\web\UploadedFile;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => ErrorAction::class,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $model = new UploadFileForm();

        $data = null;

        if ($model->load(Yii::$app->request->post())) {
            $model->file = UploadedFile::getInstance($model, 'file');
            if ($model->upload()) {
                $parser = new Parser($model->getFilePath());
                if ($parser->parse()) {
                    $data = $parser->getGraphData();
                } else {
                    Yii::$app->session->setFlash('error', 'File format error');
                }
            } else {
                Yii::$app->session->setFlash('error', 'Upload error');
            }
        }

        return $this->render('index', [
            'model' => $model,
            'data' => $data,
        ]);
    }
}

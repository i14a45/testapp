<?php


namespace app\components;


use yii\base\InvalidArgumentException;
use yii\helpers\VarDumper;

/**
 * Парсер файла
 *
 * @package app\components
 */
class Parser
{
    /** @var string */
    private $_filePath;

    /** @var array */
    private $_data;

    /**
     * @var bool
     */
    private $_startParsing = false;

    /**
     * Parser constructor.
     * @param $filePath
     */
    public function __construct($filePath)
    {
        if (!file_exists($filePath)) {
            throw new InvalidArgumentException('File not found');
        }

        $this->_filePath = $filePath;
    }

    /**
     * Парсинг файла
     *
     * @return bool
     */
    public function parse()
    {
        $dom = new \DOMDocument();
        $dom->loadHTMLFile($this->_filePath);

        $table = $dom->getElementsByTagName('table')->item(0);

        if (!$table) {
            return false;
        }

        $balance = 0;
        $data = [['Сделка', 'Баланс']];
        foreach ($table->childNodes as $index => $tr) {
            /** @var \DOMNode $tr */
            if ($tr->nodeType === XML_ELEMENT_NODE && $tr->nodeName === 'tr') {
                $this->cleanRow($tr);
                if ($this->_startParsing) {
                    $typeTd = $tr->childNodes->item(2);
                    if ($typeTd && in_array($typeTd->nodeValue, ['balance', 'buy', 'sell'])) {
                        $balance += (float) str_replace(' ', '', $tr->lastChild->nodeValue);
                        $data[] = [$index, $balance];
                    }
                } else {
                    $this->checkHeader($tr);
                }
            }
        }

        if (!$this->_startParsing) {
            return false;
        }

        $this->_data = $data;

        return true;
    }

    /**
     * Очистка строки таблицы
     *
     * @param \DOMNode $tr
     */
    protected function cleanRow(&$tr)
    {
        $toRemove = [];

        foreach ($tr->childNodes as $node) {
            /** @var \DOMNode $node */
            if ($node->nodeName !== 'td' || $node->nodeType !== XML_ELEMENT_NODE) {
                $toRemove[] = $node;
            }
        }

        foreach ($toRemove as $item) {
            $tr->removeChild($item);
        }
    }

    /**
     * Проверка начала данных по заголовку таблицы
     *
     * @param \DOMNode $el
     */
    protected function checkHeader(\DOMNode $el)
    {
        $firstTd = $el->firstChild;
        $lastTd = $el->lastChild;
        $this->_startParsing = $el->childNodes->length === 14
            && $firstTd->nodeType === XML_ELEMENT_NODE && $firstTd->nodeName === 'td' && $firstTd->nodeValue === 'Ticket'
            && $lastTd->nodeType === XML_ELEMENT_NODE && $lastTd->nodeName === 'td' && $lastTd->nodeValue === 'Profit';
    }

    /**
     * @return array
     */
    public function getGraphData()
    {
        return $this->_data;
    }
}